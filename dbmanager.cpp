#include "dbmanager.h"


/**********************************************
 *  Constructeur
 **********************************************/
DbManager::DbManager(const QString& path)
{
//  On definie notre base de donnée comme étant de type sqlite et donne son chemin d'acces
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    if (!m_db.open())
    {
      qDebug() << "Error: connection with database fail";
    }
    else
    {
    //  On récupère les noms de chaques tables
        getTablesNames();
        qDebug() << m_db.lastError();
        qDebug() << "Database: connection ok";
    }
}


/**********************************************
 *  Gère la requete permettant de faire un select avec filtre
 **********************************************/
QList<QList<QString>> DbManager::find(const QString& table,const QString& column,const QString& operation, const QString& filter)
{
//  On recupère le nombre de colonnes à matcher
    int count = getColumnsNames(table).count();

//  On prèpare la requete et injecte les différents paramètres
    QSqlQuery query;
    query.prepare("SELECT * FROM " + table + " WHERE " + column + " " + operation +" " + (filter == "" ? "0" : filter));

//  Si la requete renvoie un resultat, on entre chaques valeur dans la liste de liste result
    QList<QList<QString>> result;
    if(query.exec()) {
       while(query.next()) {
           QList<QString> row;

           for (int i = 0; i < count; ++i) {
             row << query.value(i).toString(); 
           }
           result << row;
       }
    } else {
       qDebug() << "error : "<< query.lastError();
    }

    return result;
}


/**********************************************
 *  On recupere le nom de toutes les tables
 **********************************************/
void DbManager::getTablesNames() {
  QSqlQuery query;
  query.prepare("SELECT name FROM sqlite_master");

  if(query.exec()) {
    while(query.next()) {
      this->tables.append(query.value("name").toString());
    }
  } else {
    qDebug() << "error : "<< query.lastError();
  }
}


/**********************************************
 *  On recupere le noms des colonnes d'une table donnée
 **********************************************/
QList<QString> DbManager::getColumnsNames(QString table) {
  QSqlQuery query;
  query.prepare("pragma table_info(" + table + ")");

  QList<QString> names; 
  if(query.exec()) {
    while(query.next()) {
      names.append(query.value("name").toString());
    }
  } else {
    qDebug() << "error : "<< query.lastError();
  }

  return names;
}


/**********************************************
 *  On retourne le noms des tables
 **********************************************/
QList<QString> DbManager::getTables() {
  return this->tables;
}


/**********************************************
 *  On lance la custom requete
 **********************************************/
QList<QList<QString>> DbManager::runQuery(QString sql) {
  QSqlQuery query;
  query.prepare(sql);

  QList<QList<QString>> result;

  if(query.exec()) {
    while(query.next()) {
      QList<QString> row;
      int i = 0;
      while(query.value(i).isValid()) {
        row << query.value(i).toString();
        i++;
      }
      result << row;
    }
  } else {
     qDebug() << "error : "<< query.lastError();
  }

  return result;
}


/**********************************************
 *  Requete permettant d'enregistrer des données
 **********************************************/
QList<QString> DbManager::save(QString table, QList<QString> datas) {
    QList<QString> columns = getColumnsNames(table);

//  Préparation de la requette
    QString sql;
    {
        sql.append("INSERT INTO " + table + " (");
        for( QString col: columns) {
            sql.append(col).append( ", ");
        }
        sql = sql.left(sql.length() - 2);
        sql.append( ") VALUES ('");
        for( QString data: datas) {
            sql.append( data ).append( "','" );
        }
        sql = sql.left(sql.length() - 2);
        sql.append( ")" );
    }

    QSqlQuery query;
    query.prepare(sql);

    qDebug() << sql;
    if(query.exec()) {
    while(query.next()) {
        int i = 0;
        while(query.value(i).isValid()) {
            datas << query.value(i).toString();
            i++;
        }
    }
    } else {
        qDebug() << "error : "<< query.lastError();
    }

return datas;
}
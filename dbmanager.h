#ifndef DBMANAGER_H
#define DBMANAGER_H
#include<QSqlDatabase>
#include<QDebug>
#include<QString>
#include<QSqlQuery>
#include<QSqlRecord>
#include<QSqlError>
#include<QList>

class DbManager
{
private:
    QSqlDatabase m_db;
    QList<QString> tables;

    QList<QList<QString>> columnsByTables;
    void getTablesNames();

public:
	QList<QString> getTables();
	QList<QList<QString>> runQuery(QString sql);
    QList<QString> getColumnsNames(QString table);
    QList<QString> save(QString table, QList<QString> datas);
    DbManager(const QString& path);
    QList<QList<QString>> find(const QString& table,const QString& column,const QString& operation, const QString& filter);
};

#endif // DBMANAGER_H

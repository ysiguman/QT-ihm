#include "table.h"

Table::Table(QWidget *parent) : QWidget(parent)
{
	this->table = new QTableView(parent);

	QGridLayout *layout = new QGridLayout;
	
	layout->addWidget(this->table);
    
    layout->setMargin(0);
    layout->setSpacing(0);

    this->setLayout(layout);
}

void Table::setTableData(QList<QList<QString>> datas) {
    QList<QStandardItem*> row;
    
    foreach(QList<QString> item, datas) {
    	row.clear();

    	foreach(QString col, item) {
        	row << new QStandardItem(col);
    	}
        this->model->appendRow(row);
    }

    this->table->setModel(this->model);
}

void Table::setHeaders(QList<QString> headers) {
	this->model = new QStandardItemModel;

    for (int i = 0; i < headers.count(); i++)
    {
    	this->model->setHorizontalHeaderItem(i, new QStandardItem(headers.at(i)));
    }

    this->lastColNumber = headers.count();

    this->table->setModel(this->model);
}
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include<QMainWindow>
#include<QPushButton>
#include<QDebug>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QString>
#include <QListWidget>
#include <QComboBox>
#include <QSizePolicy>
#include <QTextEdit>
#include <QFontMetrics>

#include"table.h"
#include"dbmanager.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    void save_datas();
    void on_myButton();
    void on_myButton_query();
    void on_myButton_insert();
    void on_table_change(QString);
    void on_table_change_insert(QString);

private:
    QList<QString> getData(QLayout* layout);
    void removeChildren(QLayout* layout);
    //QWidget *pageRecords;
    DbManager *db;
    Table *table;

    QList<QList<QString>> datas;

//  Filter tab 
    QComboBox *listTableWidget;
    QComboBox *listColWidget;
    QComboBox *listOperation;
    QLineEdit *filter;

//  InsertTab
    QComboBox *listTableWidgetInsert;
    QComboBox *listColWidgetInsert;
    QVBoxLayout *tableInsert;
    QString insertTable;

//  Query
    QTextEdit *query;

    void setPageRecords(QWidget *);
    void setPageInsert(QWidget *);
    void setPageQuery(QWidget *);

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H

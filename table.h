#ifndef TABLE_H
#define TABLE_H

#include<QWidget>
#include<QTableView>
#include<QStandardItemModel>
#include<QStandardItem>
#include<QDebug>
#include<QGridLayout>

class Table : public QWidget
{

private:
	QTableView *table;
	QStandardItemModel *model;
	int lastColNumber;

public:
    Table(QWidget *parent);
    void setHeaders(QList<QString>);
    void setEditLine();
    void adjustTableSize();
    void setTableData(QList<QList<QString>>);
};

#endif // TABLE_H
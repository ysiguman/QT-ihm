#include "mainwindow.h"

MainWindow::~MainWindow() {}

void MainWindow::save_datas() {
    qDebug() << this->datas;
}



/**********************************************
 *  Constructeur
 **********************************************/
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QVBoxLayout *box = new QVBoxLayout(this); // On initialise le layout principal

//  On creer l'objet permettant la gestion de la bdd
//  EN lui donnant la destination du fichier slqite
    this->db= new DbManager("/home/ysiguman/Programation/Projets - C/QT/qt_ihm_2/bdd.sqlite");

    QPushButton *saveDatas = new QPushButton("Enregistrer sous", this);

//  Gestion des onglets
//  S'occupe de la création des ongles et de 
//  l'initialisations des pages correspondantes

    // Systeme d'onglet
    QTabWidget *qtab = new QTabWidget(this);
    QWidget *pageRecords    = new QWidget(qtab);
    QWidget *pageInsert     = new QWidget(qtab);
    QWidget *pageJoin       = new QWidget(qtab);
    QWidget *pageQuery       = new QWidget(qtab);

    //  Création des pages
    setPageRecords(pageRecords);
    setPageInsert(pageInsert);
    setPageQuery(pageQuery);

    //  Mapping des pages avec les onglets
    qtab->addTab(pageRecords,  "Records");
    qtab->addTab(pageInsert,   "Insert");
    qtab->addTab(pageJoin,     "Join");
    qtab->addTab(pageQuery,     "Query");

    //  Indique que le contenue des onglets doit prendre le minimum de place possible
    qtab->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);


// Initialisation du tableau d'affichage des résultats
    this->table = new Table(this);

//  Gestion du Layout principal
//  On ajoute les différents widgets crées
    box->addWidget(qtab);
    box->addWidget(this->table);
    box->addWidget(saveDatas);

//  On affiche le layout
    setCentralWidget (new QWidget (this)) ;
    centralWidget()->setLayout(box);
    this->show();

//  On creer un "listener" qui permet de mapper le bouton save avec une méthode
    QObject::connect(saveDatas, SIGNAL (clicked()), this, SLOT (save_datas()));
}


/**********************************************
 *  Métohde appellé dans le permier onglet pour le changement de tables
 **********************************************/
void MainWindow::on_table_change(QString name) {
    this->listColWidget->clear();
    for(QString colName: this->db->getColumnsNames(name)) {
        this->listColWidget->addItem(colName);
    }
}


/**********************************************
 *  Métohde appellé lors du lancement de la recherche
 **********************************************/
void MainWindow::on_myButton() {
//  Récuperation des différents paramètres
    QString table   = this->listTableWidget->currentText();
    QString column  = this->listColWidget->currentText();
    QString operation = this->listOperation->currentText();
    QString filter = this->filter->text();

//  Récuperation de la liste des colones de la table donnée
    QList<QString> headers = db->getColumnsNames(table);

//  Appel de la bdd en focntion des paramètres selectionnés
    this->datas = db->find(table, column, operation, filter);
    
//  Préparation du tableau de résultat
    this->table->setHeaders(headers);   // On ajoute les header (nom des colonnes)
    this->table->setTableData(this->datas); // On ajoute les données récupérées
}


/**********************************************
 *  Métohde appellé pour creer l'onglet de recherche avec filtre
 **********************************************/
void MainWindow::setPageRecords(QWidget *widget) {
    QHBoxLayout *box2 = new QHBoxLayout(this);

//  Création de la liste des tables
    this->listTableWidget = new QComboBox(this);
    for(QString name: this->db->getTables()) {
        this->listTableWidget->addItem(name);
    }

//  Création de la liste des colonnes
    this->listColWidget = new QComboBox(this);
    on_table_change(this->db->getTables().at(0));

//  Création de la liste des types de comparaison
    this->listOperation = new QComboBox(this);
    this->listOperation->addItem("<");
    this->listOperation->addItem(">");
    this->listOperation->addItem("=");
    this->listOperation->addItem("!=");

//  Initialisation de l'input de filtre
    this->filter = new QLineEdit(this);
    this->filter->setPlaceholderText("Filtre");

//  Initialisation du bouton de lancement
    QPushButton *runQuery = new QPushButton(widget);
    runQuery->setText("find");

//  Mise en place du layout
    box2->addWidget(this->listTableWidget);
    box2->addWidget(this->listColWidget);
    box2->addWidget(this->listOperation);
    box2->addWidget(this->filter);
    box2->addWidget(runQuery);

//  Ajout du laout "local" à la page
    widget->setLayout(box2);
   
//  Mapping des éléments
    QObject::connect(runQuery, SIGNAL (clicked()), this, SLOT (on_myButton()));
    QObject::connect(this->listTableWidget, SIGNAL (currentIndexChanged(QString)), this, SLOT (on_table_change(QString)));
}


/**********************************************
 *  Métohde appellé dans l'onglet Insert lors du changement de table
 **********************************************/
void MainWindow::on_table_change_insert(QString name) {
    this->insertTable = name;   // On récupere le nom de la table sélectionnée
    removeChildren(this->tableInsert->layout());    //  On supprime les aciens éléments

//  On ajoute pour chaque colonnes un nouveau input
    int i = 1;
    QHBoxLayout *box = new QHBoxLayout(this);   
    for(QString colName: this->db->getColumnsNames(name)) {
        //  Si i est un multiple de 5 on créer une nouvelle ligne
        //  afin de ne pas avoir 10 éléments sur une meme lignes
        if (i % 5 == 0 )
        {
            this->tableInsert->addLayout(box);  // On ajoute l'ancienne box horizontale à la verticale
            box = new QHBoxLayout(this);        // On creer une bouvelle box horisontale
            QLineEdit *input = new QLineEdit(this); //  On creer un nouvel input qe l'on ajoute à la nouvelle box
            input->setPlaceholderText(colName);
            box->addWidget(input);
        } else {
            QLineEdit *input = new QLineEdit(this);
            input->setPlaceholderText(colName);
            box->addWidget(input);
        }
        i++;    // On incremente i pour compter le nombre d'élément ajouté
    }

    if (box->count() != 0)
    {
        this->tableInsert->addLayout(box);
    }
}


/**********************************************
 *  Métohde appellé dans l'onglet Insert lors du lancement de la recherche
 **********************************************/
void MainWindow::on_myButton_insert() {
//  Récupération des données instrites dans les inputs
    QList<QString> datas = getData(this->tableInsert->layout());

//  Lancement de la requete d'enregistrement en base
    datas = db->save(this->insertTable, datas);

//  Récuperation de la liste des colones de la table donnée
    QList<QString> headers = db->getColumnsNames(this->insertTable);

//  Mise en forme des données récupérées
    this->datas = QList<QList<QString>>();
    this->datas << datas;

//  Préparation du tableau de résultat
    this->table->setHeaders(headers);
    this->table->setTableData(this->datas);
}


/**********************************************
 *  Métohde appellé pour la création de la page Insert
 **********************************************/
void MainWindow::setPageInsert(QWidget *widget) {

    QVBoxLayout *insertLayout = new QVBoxLayout(this);
    this->tableInsert = new QVBoxLayout();

    this->listTableWidgetInsert = new QComboBox(this);

    for(QString name: this->db->getTables()) {
        this->listTableWidgetInsert->addItem(name);
    }
    on_table_change_insert(this->db->getTables().at(0));
    
    QPushButton *send = new QPushButton("send");

    insertLayout->addWidget(this->listTableWidgetInsert);
    insertLayout->addLayout(this->tableInsert);
    insertLayout->addWidget(send);

    widget->setLayout(insertLayout);

    QObject::connect(send, SIGNAL (clicked()), this, SLOT (on_myButton_insert()));
    QObject::connect(this->listTableWidgetInsert, SIGNAL (currentIndexChanged(QString)), this, SLOT (on_table_change_insert(QString)));

}


/**********************************************
 *  Métohde appellé dans l'onglet de custom Query pour le lancement de la recherche
 **********************************************/
void MainWindow::on_myButton_query() {
//  Envoie de la requete à la bdd
    this->datas = this->db->runQuery(this->query->toPlainText());

//  Préparation du tableau de résultat
    this->table->setHeaders(QList<QString>());
    this->table->setTableData(this->datas);
}


/**********************************************
 *  Métohde appellé pour la création de la page de custon Query
 **********************************************/
void MainWindow::setPageQuery(QWidget *widget) {
    QVBoxLayout *box2 = new QVBoxLayout(this);

    QPushButton *runQuery = new QPushButton(this);
    runQuery->setText("Run Query");

//  Ajout du text Edit et restrinction de celui ci à 3 lignes
    this->query = new QTextEdit(this);
    QFontMetrics m (this->query -> font()) ;
    int RowHeight = m.lineSpacing() ;
    this->query->setFixedHeight(3 * RowHeight) ;

//  Ajout des widgets au layout
    box2->addWidget(this->query);
    box2->addWidget(runQuery);

    widget->setLayout(box2);

//  Connexion du bouton avec sa méthode associée
    QObject::connect(runQuery, SIGNAL (clicked()), this, SLOT (on_myButton_query()));
}


/**********************************************
 *  Métohde appellé pour supprimer les widgets enfants d'un layout donné
 **********************************************/
void MainWindow::removeChildren(QLayout* layout) {
    QList<QWidget*> toDelete;

//  On parcour chaque enfant du layout:
//  layout->count(): le nombre d'enfants
//  layout->itemAt ( i ): on accede à l'enfant à la position i
    for (int i = 0; i < layout->count(); ++i)
    {
        //  Si l'enfant est lui meme un layout on reappelle la focntion
        //  Sinon on ajoute l'enfant à la liste d'objet à supprimer
        if (layout->itemAt ( i )->layout() != 0)
        {
            removeChildren(layout->itemAt ( i )->layout());
        } else {
            toDelete << layout->itemAt ( i )->widget();
        }
    }

//  On parcours la liste d'objet à supprimer et on les supprime de la mémoire
    for (QWidget *widget: toDelete)
    {
        delete widget;
    }
} 


/**********************************************
 *  Métohde appellé pour reccuperer les données de lineEdit d'un layout donné
 **********************************************/
QList<QString> MainWindow::getData(QLayout* layout) {
    QList<QString> data;

//  On parcour chaque enfant du layout pour en récuperer le contenu
    for (int i = 0; i < layout->count(); ++i)
    {
        if (layout->itemAt ( i )->layout() != 0)
        {
            data << getData(layout->itemAt ( i )->layout());
        } else {
            QLineEdit *line = dynamic_cast<QLineEdit*>(layout->itemAt(i)->widget()); 
            data << line->text();
        }
    }

    return data;
}
